import unittest
from Queue import Queue
from time import sleep
from unicorn_banner import UnicornBanner

# TODO: put this file outside the unit (dir with __init__.py), a dedicated test dir
class TestUnicornBanner(unittest.TestCase):

    unicorn_banner = None

    def setUp(self):
        print "Setting up"
        self.unicorn_banner = UnicornBanner("Hello World!")
        print "Finished setting up banner"

    def test_init(self):
        print "Has unicorn banner been created"
        self.assertIsNotNone(self.unicorn_banner)
        print "Is the unicorn_banner an object of class UnicornBanner"
        self.assertIsInstance(self.unicorn_banner, UnicornBanner, 'Object is instance of UnicornBanner')

    def test_run(self):
        print "Start running with initial text"
        self.unicorn_banner.start()
        print "Waiting 10 seconds"
        sleep(10)
        print "Setting new text (green)"
        self.unicorn_banner.set_text("Hello Python!", [0, 255, 0])
        print "Waiting 15 seconds"
        sleep(15)
        print "Stopping"
        self.unicorn_banner.stop()
        print "Stopped"

if __name__ == '__main__':
    unittest.main()


