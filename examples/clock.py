from unicorn_banner import UnicornBanner
from time import time, sleep, strftime, localtime
import signal

stopped = False

def tijd():
	return strftime('%H:%M', localtime(time()))

def stop(signum, frame):
	global stopped
	stopped = True


signal.signal(signal.SIGINT, stop)

if __name__ == '__main__':
	print 'Start'
	print tijd()
	banner = UnicornBanner(tijd())	
	banner.start()
	while not stopped:
		print 'Hallo'
		print tijd()
		banner.set_text(tijd())
		sleep(20)
	

banner.stop()



